package eu.semfact.webid.resteasy;

import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.tdb.TDBFactory;
import com.hp.hpl.jena.util.FileManager;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.interfaces.RSAPublicKey;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.ext.Provider;
import org.jboss.resteasy.annotations.interception.Precedence;
import org.jboss.resteasy.annotations.interception.ServerInterceptor;
import org.jboss.resteasy.core.ResourceMethod;
import org.jboss.resteasy.core.ServerResponse;
import org.jboss.resteasy.spi.Failure;
import org.jboss.resteasy.spi.HttpRequest;

@Provider
@ServerInterceptor
@Precedence("SECURITY")
public class TDBWebIDInterceptor extends SimpleWebIDInterceptor {

    static final String DATEPROP = "http://purl.org/dc/terms/date";
    protected int cacheTTL = 60;
    protected SimpleDateFormat xsddate = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
    protected String path;
    
    public TDBWebIDInterceptor() {
        this("session");
    }
    
    public TDBWebIDInterceptor(String path) {
        this.path = path;
    }

    @Override
    public ServerResponse preProcess(HttpRequest req, ResourceMethod m) throws Failure, WebApplicationException {
        Dataset session = TDBFactory.createDataset(this.path);
        Model webid;
        RSAPublicKey rsa;
        PublicKey key;
        Certificate cert = (Certificate) req.getAttribute("webidcertificate");
        Collection<String> claims = (Collection<String>) req.getAttribute("webidclaims");
        if (claims == null || cert == null) {
            return null;
        }
        key = cert.getPublicKey();
        if (key.getAlgorithm().equals("RSA")) {
            rsa = (RSAPublicKey) key;
        } else {
            return null;
        }
        List<String> webids = new ArrayList<>();
        for (String claim : claims) {
            boolean contains;
            session.begin(ReadWrite.WRITE);
            try {
                contains = session.containsNamedModel(claim);
                if (contains) {
                    String query = getCacheQuery(claim);
                    QueryExecution ask = QueryExecutionFactory.create(query, session.getDefaultModel());
                    boolean cached = ask.execAsk();
                    if (cached) {
                        webid = session.getNamedModel(claim);
                    } else {
                        webid = FileManager.get().loadModel(claim);
                        session.removeNamedModel(claim);
                        session.addNamedModel(claim, webid);
                        this.setDateNow(session.getDefaultModel(), claim);
                    }
                } else {
                    webid = FileManager.get().loadModel(claim);
                    session.addNamedModel(claim, webid);
                    this.setDateNow(session.getDefaultModel(), claim);
                }

                session.commit();
                session.begin(ReadWrite.READ);
                String query = getWebIDQuery(claim, rsa.getModulus(), rsa.getPublicExponent());
                QueryExecution ask = QueryExecutionFactory.create(query, webid);
                boolean verified = ask.execAsk();
                if (verified) {
                    webids.add(claim);
                }
            } finally {
                session.end();
            }
        }
        req.setAttribute("webids", webids);
        return null;
    }

    protected void setDateNow(Model m, String uri) {
        Resource s = ResourceFactory.createResource(uri);
        Property p = ResourceFactory.createProperty(DATEPROP);
        RDFNode o = ResourceFactory.createTypedLiteral(xsddate.format(new Date()), XSDDatatype.XSDdateTime);
        m.remove(s, p, o);
        m.add(s, p, o);
    }

    protected String getCacheQuery(String uri) {
        long since = new Date().getTime() - cacheTTL * 1000;
        Date sinceDate = new Date(since);
        String sinceStr = xsddate.format(sinceDate);
        return "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n"
                + "PREFIX dc: <http://purl.org/dc/terms/>\n"
                + "ASK {\n"
                + "    <" + uri + "> dc:date ?d\n"
                + "    FILTER (?d > \"" + sinceStr + "\"^^xsd:dateTime)\n"
                + "}\n";
    }
}
